from turtle import Turtle, Screen
import random as r


s = Screen()
s.setup(width=500, height=400)
player_choice = s.textinput("Pick a turtle!", "[red/blue/green]")
is_race_on = False
colors = ["red", "blue", "green"]
racers = []
x_finish_line = -200
y_position_list = [100, 0, -100]

for turtle_index in range(0, len(colors)):
    t = Turtle(shape="turtle")
    t.color(colors[turtle_index])
    t.penup()
    t.goto(x_finish_line, y_position_list[turtle_index])
    racers.append(t)

is_race_on = True
while is_race_on:
    for racer in racers:
        if racer.xcor() > 230:
            is_race_on = False
            winning_color = racer.pencolor()
            if winning_color == player_choice:
                print(winning_color, " ==> You win")
            else:
                print(winning_color, " ==> You lose (", player_choice, ")")

        racer.forward(r.randint(1, 10))
s.exitonclick()
